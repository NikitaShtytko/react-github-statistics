import { Link } from "react-router-dom";

function Navbar() {
  return (
    <nav className="navbar">
      <Link to="/">Home</Link>
      <div>
        <h2>GitHub Statistics</h2>
      </div>
    </nav>
  );
}

export default Navbar;