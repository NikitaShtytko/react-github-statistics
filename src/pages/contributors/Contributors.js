import React, { useEffect, useState } from "react";
import axios from "axios";
import HighchartsReact from "highcharts-react-official";
import Highcharts from 'highcharts';
import { Link, useParams } from "react-router-dom";
import "./Contributors.scss"

function Contributors() {
  let { userName, project } = useParams();
  const [contributors, setContributors] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios.get(`https://api.github.com/repos/${userName}/${project}/stats/contributors`)
      .then(response => setContributors(response.data))
      .catch(error => {
        setError(error?.response?.data?.message)
        console.error(error)
      });
  }, [userName, project])

  const options = {
    title: {
      text: 'Contributions per week',
    },
    xAxis: {
      categories: [],
    },
    yAxis: {
      title: {
        text: 'Number of contributions',
      },
    },
    series: [
      {
        name: 'Contributors',
        data: [],
      },
    ],
  };

  if (contributors.length) {
    options.xAxis.categories = contributors[0].weeks.map(
      (week) => new Date(week.w * 1000).toLocaleDateString()
    );
    options.series[0].data = contributors.map(
      (contributor) => contributor.weeks.reduce((sum, week) => sum + week.c, 0)
    );
  }

  return (
    <div className="contributors">
      <button className="button contributors__back-btn">
        <Link to={`/projects/${userName}`}>Back</Link>
      </button>
      <p className="text">Contributors</p>
      {
        error
        ? <p className="text text_error">{error}</p>
        : <HighchartsReact highcharts={Highcharts} options={options}/>
      }
    </div>
  )
}

export default Contributors;