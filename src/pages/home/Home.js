import React, {useState} from "react";
import {useNavigate} from 'react-router-dom';
import './Home.scss';

function Home() {
  let navigate = useNavigate();

  const [userName, setUserName] = useState("reworkd");

  const submit = () => {
    if (userName.length)
      navigate(`projects/${userName}`)
  }

  return (
    <div className="home">
      <div className="home__message">
        <p className="text">Welcome to the Github statistics app!</p>
        <p className="text">Select a project from the Projects page to view its contributions.</p>
      </div>
      <div className="home__search">
        <input
          className="input"
          type="text"
          required={true}
          value={userName}
          onChange={(e) => setUserName(e.target.value)}
        />
        <button onClick={() => submit()} className="button">Projects</button>
      </div>
    </div>
  )
}

export default Home;