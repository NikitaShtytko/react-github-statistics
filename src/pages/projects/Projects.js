import { useEffect, useState } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";
import "./Projects.scss"

function Projects() {
  let { userName } = useParams();
  const [projects, setProjects] = useState([]);
  const [error, setError] = useState(null);

  useEffect(() => {
    axios.get(`https://api.github.com/users/${userName}/repos`)
      .then(response => setProjects(response.data))
      .catch(error => {
        setError(error?.response?.data?.message)
        console.error(error);
      })
  }, [userName]);

  return (
    <div className="projects">
      <p className="text">{userName} projects</p>
      {error && <p className="text text_error">{error}</p>}
      <div className="projects__sidebar">
        {projects.map((project) => (
          <button className="button projects__button" key={project.id}>
            <Link to={`/contributors/${userName}/${project.name}`}>{project.name}</Link>
          </button>
        ))}
      </div>
    </div>
  );
}

export default Projects;