import './App.scss';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import React from "react";
import Home from "./pages/home/Home";
import Navbar from "./components/Navbar/Navbar";
import Projects from "./pages/projects/Projects";
import Contributors from "./pages/contributors/Contributors";

function App() {
  return (
    <BrowserRouter>
      <Navbar/>
      <Routes>
        <Route path="/" element={<Home/>}/>
          <Route path="/projects/:userName" element={<Projects/>}/>
          <Route path="/contributors/:userName/:project" element={<Contributors/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
