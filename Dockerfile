FROM node:18
WORKDIR /usr/src
COPY . ./
RUN npm install
EXPOSE 3080

CMD ["npm", "run", "start"]